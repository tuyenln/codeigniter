<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('customer','customer');
	}

	public function index(){
		$this->load->helper('url');
		$this->load->view('customers/customer');
	}

	public function listCustomer(){
		$this->load->helper('url');

		$list = $this->customer->getDatatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $customer) {
			$no++;
			$row = array();
			$row[] = '<input type="checkbox" class="data-check" value="'.$customer->id.'">';
			$row[] = $customer->name;
			$row[] = $customer->email;
			$row[] = $customer->create_date;
			if(empty($customer->update_date)){
				$row[] = $customer->create_date;
			}else{
				$row[] = $customer->update_date;
			}

			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="editCustomer('."'".$customer->id."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="deleteCustomer('."'".$customer->id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->customer->countAll(),
						"recordsFiltered" => $this->customer->countFiltered(),
						"data" => $data,
				);
		echo json_encode($output);
	}

	public function edit($id){
		$data = $this->customer->getById($id);
		echo json_encode($data);
	}

	public function add(){
		$this->validateForm();
		
		$data = array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
			);

		$insert = $this->customer->save($data);

		echo json_encode(array("status" => true));
	}

	public function update(){
		$this->validateForm();
		$data = array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
			);
		$this->customer->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => true));
	}

	public function delete($id){
		$this->customer->deleteById($id);
		echo json_encode(array("status" => true));
	}

	public function deleteAll(){
		$list_id = $this->input->post('id');
		foreach ($list_id as $id) {
			$this->customer->deleteById($id);
		}
		echo json_encode(array("status" => true));
	}

	private function validateForm(){

		$this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');


		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = true;

		if($this->input->post('name') == ''){
			$data['inputerror'][] = 'name';
			$data['error_string'][] = 'Name is required';
			$data['status'] = false;
		}

		if($this->input->post('email') == ''){
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Email is required';
			$data['status'] = false;
		}else{
			if($this->input->post('id') == ''){
				if($this->customer->getEmail($this->input->post('email'))) {
					$data['inputerror'][] = 'email';
					$data['error_string'][] = 'Email is exists';
					$data['status'] = false; 
				}
			}else{
				$oldEmail = $this->customer->getEmailById($this->input->post('id'));
				if($this->input->post('email') != $oldEmail->email){
					if($this->customer->getEmail($this->input->post('email'))) {
						$data['inputerror'][] = 'email';
						$data['error_string'][] = 'Email is exists';
						$data['status'] = false; 
					}
				}
			}
			if($this->form_validation->run() === false){
				$data['inputerror'][] = 'email';
				$data['error_string'][] = strip_tags(validation_errors());
				$data['status'] = false; 
			}
		}

		if($data['status'] === false){
			echo json_encode($data);
			exit();
		}
	}

}

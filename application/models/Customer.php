<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Model {

	var $table = 'customers';
	var $columnOrder = array(null,'name','email','create_date','update_date',null);
	var $columnSearch = array('name','email','update_date'); 
	var $order = array('id' => 'desc'); // default order 

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	private function getDbQuery(){
		
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->columnSearch as $item) {
			if($_POST['search']['value']){
				
				if($i===0){
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->columnSearch) - 1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->columnOrder[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function getDatatables(){
		$this->getDbQuery();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function countFiltered(){
		$this->getDbQuery();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function countAll(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function getById($id){
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data){
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function deleteById($id){
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function getEmail($email){
		$this->db->from($this->table);
		$this->db->where('email', $email);
		$query = $this->db->get();
		if( $query->num_rows() > 0 ){
			return true; 
		}else{
			return false; 
		}
	}

	public function getEmailById($id){
		$this->db->select('email'); 
		$this->db->from($this->table);
		$this->db->where('id',$id);
		return $this->db->get()->row();
	}

}

var saveMethod;
var table;
var baseUrl = 'http://127.0.0.1/codeigniter/index.php';

    $(document).ready(function() {

        table = $('#table').DataTable({ 

            "processing": true,
            "serverSide": true,
            "order": [],

            "ajax": {
                "url": baseUrl+ "/customers/listCustomer",
                "type": "POST"
            },

            "columnDefs": [
                { 
                    "targets": [ 0 ], 
                    "orderable": false, 
                },
                { 
                    "targets": [ -1 ], 
                    "orderable": false, 
                },
            ],

        });


        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("#check-all").click(function () {
            $(".data-check").prop('checked', $(this).prop('checked'));
        });

    });



    function addCustomer(){
        saveMethod = 'add';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show');
        $('.modal-title').text('Add Customer');
        // $('[name="email"]').prop('readonly', false);

    }

    function editCustomer(id){
        saveMethod = 'update';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : baseUrl + "/customers/edit/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data){
                if(saveMethod === 'update'){
                    // $('[name="email"]').prop('readonly', true);
                }
                $('[name="id"]').val(data.id);
                $('[name="name"]').val(data.name);
                $('[name="email"]').val(data.email);
                $('#modal_form').modal('show');
                $('.modal-title').text('Edit Customer');
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    }

    function reloadTable(){
        table.ajax.reload(null,false);
    }

    function save(){
        $('#btnSave').text('saving...');
        $('#btnSave').attr('disabled',true);
        var url;

        if(saveMethod == 'add') {
            url = baseUrl + "/customers/add";
        }else{
            url = baseUrl + "/customers/update";
        }
        var formData = new FormData($('#form')[0]);
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data){
                if(data.status){
                    $('#modal_form').modal('hide');
                    reloadTable();
                }else{
                    for (var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }
                $('#btnSave').text('save');
                $('#btnSave').attr('disabled',false);


            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error adding / update data');
                $('#btnSave').text('save');
                $('#btnSave').attr('disabled',false);

            }
        });
    }

    function deleteCustomer(id){
        if(confirm('Are you sure delete record?')){
            $.ajax({
                url : baseUrl + "/customers/delete/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data){
                    $('#modal_form').modal('hide');
                    reloadTable();
                },
                error: function (jqXHR, textStatus, errorThrown){
                    alert('Error deleting data');
                }
            });

        }
    }

    function deleteCustomers(){
        var listId = [];
        $(".data-check:checked").each(function() {
                listId.push(this.value);
        });
        if(listId.length > 0){
            if(confirm('Are you sure delete '+listId.length+' records?')){
                $.ajax({
                    type: "POST",
                    data: {id:listId},
                    url: baseUrl + "/customers/deleteAll",
                    dataType: "JSON",
                    success: function(data){
                        if(data.status){
                            reloadTable();
                        }else{
                            alert('Failed.');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        alert('Error deleting data');
                    }
                });
            }
        }
        else{
            alert('no data selected');
        }
    }
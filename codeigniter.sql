-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: codeigniter
-- ------------------------------------------------------
-- Server version	5.7.9

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (17,'tuyenlnse','tuyenlnse@gmail.com','2016-09-19 23:15:28','2016-09-19 23:51:22'),(18,'tuyen test','tuyenlnse+1@gmail.com','2016-09-19 23:16:00',NULL),(19,'test1','tuyenlnse+2@gmail.com','2016-09-19 23:16:26',NULL),(20,'test2','tuyenlnse+3@gmail.com','2016-09-19 23:18:55',NULL),(21,'tuyen le','tuyenlnse+4@gmail.com','2016-09-19 23:19:16',NULL),(22,'Test 4','tuyenlnse+5@gmail.com','2016-09-19 23:19:37',NULL),(23,'test5','tuyenlnse+6@gmail.com','2016-09-19 23:19:53',NULL),(24,'test7','tuyenlnse+7@gmail.com','2016-09-19 23:20:07',NULL),(25,'Test 8','tuyenlnse+8@gmail.com','2016-09-19 23:20:38',NULL),(26,'test 9','tuyenlnse+909@gmail.com','2016-09-19 23:20:51','2016-09-20 00:18:33'),(27,'test 10','tuyenlnse+10@gmail.com','2016-09-19 23:21:06',NULL),(28,'tuyen','tuyenlnse+9@gmail.com','2016-09-19 23:27:02','2016-09-19 23:32:46'),(30,'kakaazzz','tuyenlnse+101@gmail.com','2016-09-19 23:47:24','2016-09-19 23:57:03'),(31,'tuyenbb','tuyenlnse+999@gmail.com','2016-09-19 23:57:20','2016-09-20 00:19:24'),(32,'zzzz','test@123.com','2016-09-20 10:19:16',NULL),(33,'aaa','fefef@gmail.com','2016-09-20 10:33:18',NULL),(34,'tuyen','eeeefff@gmail.com','2016-09-20 11:44:19',NULL),(35,'zzz','zzz@gmail.com','2016-09-20 12:19:56',NULL);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-20 14:28:15
